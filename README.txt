/* $Id$ */

Yahoo Media Player module that adds the Yahoo Media Player to
pages that provide links to mp3s or videos.

Installation
------------

Enable the module and visit http://example.com/admin/yahoomediaplayer/settings
to choose which node types will utilize the player.
The player appears when there is linked media content on a node.

Author
-------
Robbie Ferrero
robbieferrero@gmail.com