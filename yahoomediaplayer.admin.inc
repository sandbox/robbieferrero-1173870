<?php

// $Id$

/**
 * @file
 * This file provides a form to select node types to include Yahoo Media Player functionality
 *
 */
 
function yahoomediaplayer_admin_settings() {

  $types = node_get_types('names');

  $form['yahoomediaplayer_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose node types to apply media player to.'),
    '#options' => $types,
    '#default_value' => variable_get('yahoomediaplayer_node_types', array('page')),
    '#description' => t('The yahoo media player will be displayed only on selected node types if they contain media'),
  );
  return system_settings_form($form);
}
